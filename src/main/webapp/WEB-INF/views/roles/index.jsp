<%-- 
    Document   : index
    Created on : 15.01.2013, 21:56:46
    Author     : comrad
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello Roles!</h1>
        <ul>
            <c:forEach items="${roles}" var="role">
                <li>
                    ${role.name}
                </li>
            </c:forEach>
        </ul>
    </body>
</html>
