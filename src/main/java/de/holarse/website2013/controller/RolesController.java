/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.holarse.website2013.controller;

import de.holarse.website2013.model.dao.RolesDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author comrad
 */
@Controller
@RequestMapping("roles")
public class RolesController {

    @Autowired RolesDao rolesDao;
    
    @RequestMapping("index")
    public ModelAndView doIndex() {
        final ModelAndView mav = new ModelAndView("roles/index");
        mav.addObject("roles", getRolesDao().getAll());
        return mav;
    }

    public RolesDao getRolesDao() {
        return rolesDao;
    }

    public void setRolesDao(RolesDao rolesDao) {
        this.rolesDao = rolesDao;
    }
    
}
