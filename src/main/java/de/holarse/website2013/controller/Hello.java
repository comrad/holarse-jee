/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.holarse.website2013.controller;

import java.util.logging.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author comrad
 */
@Controller
@RequestMapping("/sample")
public class Hello {
    private static final Logger LOG = Logger.getLogger(Hello.class.getName());
    
    /**
     * Beispielseite
     * @return 
     */
    @RequestMapping(value = "index.html", method=RequestMethod.GET)
    public ModelAndView sayHello() {
        final ModelAndView mav = new ModelAndView("index");
        mav.addObject("lala", "rhababer");
        LOG.fine("hallo test logging");
        return mav;
    }
    
}
