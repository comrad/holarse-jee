/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.holarse.website2013.model.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author comrad
 */
public abstract class DaoSupport {
    
    @Autowired private SessionFactory sessionFactory;
    
    public SessionFactory getSessionFactory() {
        return this.sessionFactory;
    }
    
}
