/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.holarse.website2013.model.dao;

import de.holarse.website2013.model.domain.Role;
import java.util.List;

/**
 *
 * @author comrad
 */
public interface RolesDao {

    List<Role> getAll();
    
}
