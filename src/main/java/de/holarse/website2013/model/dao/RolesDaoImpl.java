/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.holarse.website2013.model.dao;

import de.holarse.website2013.model.domain.Role;
import java.util.List;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Service;

/**
 *
 * @author comrad
 */
@Service("rolesDao")
public class RolesDaoImpl extends DaoSupport implements RolesDao {

    @Override
    public List<Role> getAll() {
        return getSessionFactory().openSession().createCriteria(Role.class).addOrder(Order.asc("name")).list();
    }
    
}
